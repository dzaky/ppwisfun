from django.db import models
from datetime import datetime, date
from django.utils import timezone

# Create your models here.
class StatusAnda(models.Model):
    status = models.CharField(max_length = 1000)
    time = models.DateTimeField(auto_now_add=True)

    def ___str__(self):
        return self.status