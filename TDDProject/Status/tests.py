from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import StatusAnda
from .forms import StatusForm

from .views import index_status

# Create your tests here.
class Story6Test(TestCase):
	def test_url_exist(self):
		response = Client().get('/status')
		self.assertEqual(response.status_code, 200)

	def test_function_caller_exist(self):
		found = resolve('/status')
		self.assertEqual(found.func, index_status)

	def test_landingpage_containt(self):
		response = self.client.get('/status')
		html_response = response.content.decode('utf8')
		self.assertIn('Hello, Apa Kabar?', html_response)

	def test_can_create_new_status(self):
		testText = 'Lorem Ipsum'
		new_act = StatusAnda.objects.create(status= testText)
		counting_all_available_status =  StatusAnda.objects.all().count();
		self.assertEqual(counting_all_available_status, 1)

	def test_form_Status_rendered(self):
		response = Client().get('/status')
		html_response = response.content.decode('utf8')
		self.assertIn('status', html_response)
